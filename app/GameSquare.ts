export class GameSquare {
    mine: boolean;
    flag: boolean;
    adjacentMines: number;
}