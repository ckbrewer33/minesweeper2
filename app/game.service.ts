import { Injectable } from '@angular/core';

import { GAME1 } from './GameData';
import { GAME2 } from './GameData';
import { GAME3 } from './GameData';
import { GameSquare } from './GameSquare';

@Injectable()
export class GameService {
    getGame1() {
        return Promise.resolve(GAME1);
    }
    
    getGame2() {
        return Promise.resolve(GAME2);
    }
    
    getGame3() {
        return Promise.resolve(GAME3);
    }
}