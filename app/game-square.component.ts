import { Component, Input } from '@angular/core';

@Component ({
    selector: 'game-square',
    template: `
        <a href="#" (click)=leftClick() class="gameSquare" 
            [class.defaultSquare]="!revealed" 
            [class.mineSquare]="revealed && mine"
            [class.square1]="revealed && adjacentMines === 1" 
            [class.square2]="revealed && adjacentMines === 2"
            [class.square3]="revealed && adjacentMines === 3"
            [class.square4]="revealed && adjacentMines === 4"
            [class.square5]="revealed && adjacentMines === 5"
            [class.square6]="revealed && adjacentMines === 6"
            [class.square7]="revealed && adjacentMines === 7"
            [class.square8]="revealed && adjacentMines === 8" 
            >
        </a>
    `,
    styles: [`
        .gameSquare {
            display: inline-block;
            height: 32px;
            width: 32px;
            border: 1px solid #8C8C8C;
        }
        .mineSquare {
            background-image: url(images/mine32.png);
        }
        .defaultSquare {
            background-image: url(images/default.png);
        }
        .square1 {
            background-image: url(images/1.png);
        }
        .square2 {
            background-image: url(images/2.png);
        }
        .square3 {
            background-image: url(images/3.png);
        }
        .square4 {
            background-image: url(images/4.png);
        }
        .square5 {
            background-image: url(images/5.png);
        }
        .square6 {
            background-image: url(images/6.png);
        }
        .square7 {
            background-image: url(images/7.png);
        }
        .square8 {
            background-image: url(images/8.png);
        }
    `]
})

export class GameSquareComponent{
    @Input()
    mine: boolean;
    
    @Input()
    flag: boolean;
    
    @Input()
    adjacentMines: number;
    
    revealed: boolean;
    
    constructor() {
        this.revealed = false;
    }
    
    leftClick() {
        this.revealed = true;
    }
}