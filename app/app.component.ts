import { Component } from '@angular/core';

import { GameComponent } from './game.component';

@Component({
    selector: 'my-app',
    template: `
        <h2 class="title">Minesweeper</h2>
        <game-grid></game-grid>
    `,
    styles: [`
      .title {
          color: #369;
          font-family: Arial, Helvetica, sans-serif;
          margin-left: auto;
          margin-right: auto;
          text-align: center;
      }
    `],
    directives: [GameComponent]
})

export class AppComponent { }
