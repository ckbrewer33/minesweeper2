import { Component, OnInit } from '@angular/core';

import { GameSquareComponent } from './game-square.component';
import { GameService } from './game.service';
import { GameSquare } from './GameSquare';

@Component ({
    selector: 'game-grid',
    template: `
        <div class="gameGrid" >
            <game-square *ngFor="let square of game" 
                [mine]="square.mine" 
                [flag]="square.flag"
                [adjacentMines]="square.adjacentMines"
                >
            </game-square>
        </div>
    `,
    styles: [`
        .gameGrid {
            border: 3px solid gray;
            width: 186px;
            margin: auto;
            padding: 8px;
        }
    `],
    directives: [GameSquareComponent],
    providers: [GameService]
})

export class GameComponent implements OnInit {
    game: GameSquare[];
    
    constructor(private gameService: GameService) { }
    
    ngOnInit() {
        this.getGame1();
        // this.getGame2();
        // this.getGame3();
    }
    
    getGame1() {
        this.gameService.getGame1().then(game => this.game = game);
    }
    
    getGame2() {
        this.gameService.getGame2().then(game => this.game = game);
    }
    
    getGame3() {
        this.gameService.getGame3().then(game => this.game = game);
    }
}