import { GameSquare } from './GameSquare';

export var GAME1: GameSquare[] = [
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 0},
    {"mine":false,  "flag": false,  "adjacentMines": 0},
    
    {"mine":false,  "flag": false,  "adjacentMines": 3},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 3},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 3},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    
    {"mine":false,  "flag": false,  "adjacentMines": 0},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 1}
];

export var GAME2: GameSquare[] = [
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 0},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 0},
    {"mine":false,  "flag": false,  "adjacentMines": 0},
    
    {"mine":false,  "flag": false,  "adjacentMines": 3},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":true,   "flag": false,  "adjacentMines": -1}
];

export var GAME3: GameSquare[] = [
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 4},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    
    {"mine":false,  "flag": false,  "adjacentMines": 3},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 5},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 4},
    
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 3},
    {"mine":false,  "flag": false,  "adjacentMines": 4},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 5},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    
    {"mine":false,  "flag": false,  "adjacentMines": 0},
    {"mine":false,  "flag": false,  "adjacentMines": 1},
    {"mine":false,  "flag": false,  "adjacentMines": 2},
    {"mine":true,   "flag": false,  "adjacentMines": -1},
    {"mine":false,  "flag": false,  "adjacentMines": 2}
];